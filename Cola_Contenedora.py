import Cola_de_Procesos

class Cola_contenedora:
    def __init__(self):
        self.cola_critica = Cola_de_Procesos.Cola_de_Procesos()
        self.cola_no_critica = Cola_de_Procesos.Cola_de_Procesos()
        self.contador_cola_critica = 0

    def encolar(self, proceso_nuevo):
        if proceso_nuevo.get_valor_critico() == 1:
            self.cola_critica.encolar(proceso_nuevo) 
        else:
            self.cola_no_critica.encolar(proceso_nuevo)
            
    def ejecutar(self):
        proceso_a_ejecutar = None
        if self.contador_cola_critica < 2:
            # Si el contador es menor que dos, primero busco en críticos y, si no hay críticos, busco en no críticos.
            if self.cola_critica.tamanio > 0:
                proceso_a_ejecutar = self.cola_critica.desencolar()
                self.contador_cola_critica += 1
            else:  
                # Pimero mirás la cola de no críticos. Si encontraste, sacás uno no crítico y el contador vuelve a 0
                # Si no había nada en los no críticos, vas a los críticos... pero acá el contador no vuelve a 0  
                if self.cola_no_critica.tamanio > 0:
                    proceso_a_ejecutar = self.cola_no_critica.desencolar()
        else:
            # Primero te fijás en la cola de no críticos... si hay, desencolás y el contador vuelve a 0
            # Si no, vas a la cola de críticos, pero el contador no cambia....
            if self.cola_no_critica.tamanio > 0:
                proceso_a_ejecutar = self.cola_no_critica.desencolar()
                self.contador_cola_critica = 0
            elif self.cola_critica.tamanio > 0:
                    proceso_a_ejecutar = self.cola_critica.desencolar()
        print(proceso_a_ejecutar.id)