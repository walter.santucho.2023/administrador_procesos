import proceso

class Lista_de_procesos_no_criticos:
    def __init__(self):
        self.primero = None
        self.tamanio = 0

    def get_tamanio(self):
        return self.tamanio

    def insertar_proceso_no_critico(self, proceso_nuevo):
        if self.primero is None:
            self.primero = proceso_nuevo
        else:
            actual = self.primero
            while actual.derecha:
                actual = actual.derecha
            actual.derecha = proceso_nuevo
        self.tamanio += 1
        
    def buscar_proceso_no_critico(self, id):
        actual = self.primero
        while actual:
            if actual.get_id() == id:
                return actual
            actual = actual.derecha
        return None
    
    def mostrar_lista(self):
        actual = self.primero
        while actual:
            print(str(actual.id) + " " + actual.nombre_proceso + " " + \
                  str(actual.valor_critico) + " " + str(actual.memoria) + " " + \
                  str(actual.tiempo_ejecucion) + " " + str(actual.parametros) \
                      ,end="\n")
            actual = actual.derecha
        