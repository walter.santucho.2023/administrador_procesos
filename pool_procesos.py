import arbol_de_procesos_criticos
import lista_procesos_no_criticos
import proceso

class Pool_Procesos:
    def __init__(self):
        self.arbol_criticos = arbol_de_procesos_criticos.Arbol_procesos_criticos()
        self.lista_no_criticos = lista_procesos_no_criticos.Lista_de_procesos_no_criticos()

    def buscar_proceso(self, id_proceso, valor_critico):
        proceso_actual = proceso.Proceso()   
        if (valor_critico == 1):
            proceso_actual = self.arbol_criticos.buscar_proceso_critico(id_proceso)
        else:
            proceso_actual = self.lista_no_criticos.buscar_proceso_no_critico(id_proceso)

        if proceso_actual != None:    
            return proceso_actual.crear_copia()
        else:
            return None

    def insertar_proceso(self, proceso_nuevo):
        if (proceso_nuevo.get_valor_critico() == 1):
            self.arbol_criticos.insertar_proceso_critico(proceso_nuevo)
        else:
            self.lista_no_criticos.insertar_proceso_no_critico(proceso_nuevo)