import pool_procesos
import Cola_Contenedora
import proceso

def crear_proceso(nombre, id, memoria, tiempo_de_ejecucion, criticidad):
    nuevo_proceso = proceso.Proceso()
    nuevo_proceso.nombre_proceso = nombre
    nuevo_proceso.id = id
    nuevo_proceso.memoria = memoria
    nuevo_proceso.tiempo_ejecucion = tiempo_de_ejecucion
    nuevo_proceso.valor_critico = criticidad
    nuevo_proceso.izquierda = None
    nuevo_proceso.derecha = None
    return nuevo_proceso
    
def Unipe_CABA_Los_Mas_Grandes():
    pool = pool_procesos.Pool_Procesos()
    cola_contenedora = Cola_Contenedora.Cola_contenedora()
    proceso_actual = proceso.Proceso()
    
    #crear procesos criticos y guardar en pool de procesos
    proceso_actual = crear_proceso('Mover el mouse', 101, 32, 50, 1)
    pool.insertar_proceso(proceso_actual)
    
    proceso_actual = crear_proceso('Escritura de teclado', 100, 32, 50, 1)
    pool.insertar_proceso(proceso_actual)
    
    proceso_actual = crear_proceso('Regular temperatura del equipo', 102, 32, 50, 1)
    pool.insertar_proceso(proceso_actual)
    
    #crear procesos no criticos y guardar en pool de procesos
    proceso_actual = crear_proceso('Reproducir un video', 200, 32, 50, 2)
    proceso_actual.parametros = {"ruta_archivo: " : "video1.mp4"}
    pool.insertar_proceso(proceso_actual) 
    
    proceso_actual = crear_proceso('Imprimir un archivo', 201, 32, 50, 2)
    proceso_actual.parametros = {"ruta_archivo: " : "libro1.pdf"}
    pool.insertar_proceso(proceso_actual) 
    
    proceso_actual = crear_proceso('Abrir una imagen', 202, 32, 50, 2)
    proceso_actual.parametros = {"ruta_archivo: ": "imagen.jpg"}
    pool.insertar_proceso(proceso_actual) 
    
    proceso_actual = crear_proceso('Descargar un archivo', 203, 32, 50, 2)
    proceso_actual.parametros = {"ruta_archivo: " : "url"}
    pool.insertar_proceso(proceso_actual) 
    
    proceso_actual = crear_proceso('Reproducir un audio', 204, 32, 50, 2)
    proceso_actual.parametros = {"ruta_archivo: " : "cancion1.mp3"}
    pool.insertar_proceso(proceso_actual) 
    
    proceso_actual = crear_proceso('Eliminar archivo', 205, 32, 50, 2)
    proceso_actual.parametros = {"ruta_archivo: " : "dato1.dat"}
    pool.insertar_proceso(proceso_actual) 
    
    proceso_actual = crear_proceso('Consultar base de datos', 206, 32, 50, 2)
    proceso_actual.parametros = {"ruta_archivo: " : "datos.mdb"}
    pool.insertar_proceso(proceso_actual) 
    
    proceso_actual = crear_proceso('Ejecutar videojuego', 207, 32, 50, 2)
    proceso_actual.parametros = {"ruta_archivo: " : "juego1.dat"}
    pool.insertar_proceso(proceso_actual)
    
    """   print(pool.arbol_criticos.get_tamanio())
    print(pool.lista_no_criticos.get_tamanio())
    
    print("\n Procesos criticos")
    pool.arbol_criticos.inorden(pool.arbol_criticos.raiz)
    print("\n Procesos no criticos")
    pool.lista_no_criticos.mostrar_lista() """
    
    #encolar procesos criticos en cola contenedora
    proceso_actual = pool.buscar_proceso(101, 1)   
    #proceso_actual.mostrar_proceso()
    if proceso_actual:
        cola_contenedora.encolar(proceso_actual)
    
    proceso_actual = pool.buscar_proceso(100, 1)   
    #proceso_actual.mostrar_proceso()
    if proceso_actual:
        cola_contenedora.encolar(proceso_actual)
        
    proceso_actual = pool.buscar_proceso(102, 1)   
    #proceso_actual.mostrar_proceso()
    if proceso_actual:
        cola_contenedora.encolar(proceso_actual)
    
    #encolar procesos no criticos en cola contenedora    
    proceso_actual = pool.buscar_proceso(200, 2)   
    #proceso_actual.mostrar_proceso()
    if proceso_actual:
        cola_contenedora.encolar(proceso_actual)
        
    proceso_actual = pool.buscar_proceso(201, 2)   
    #proceso_actual.mostrar_proceso()
    if proceso_actual:
        cola_contenedora.encolar(proceso_actual)
    
    proceso_actual = pool.buscar_proceso(202, 2)   
    #proceso_actual.mostrar_proceso()
    if proceso_actual:
        cola_contenedora.encolar(proceso_actual)
    
    proceso_actual = pool.buscar_proceso(203, 2)   
    #proceso_actual.mostrar_proceso()
    if proceso_actual:
        cola_contenedora.encolar(proceso_actual)
    
    proceso_actual = pool.buscar_proceso(204, 2)   
    #proceso_actual.mostrar_proceso()
    if proceso_actual:
        cola_contenedora.encolar(proceso_actual)
    
    proceso_actual = pool.buscar_proceso(205, 2)   
    #proceso_actual.mostrar_proceso()
    if proceso_actual:
        cola_contenedora.encolar(proceso_actual)
    
    proceso_actual = pool.buscar_proceso(206, 2)   
    #proceso_actual.mostrar_proceso()
    if proceso_actual:
        cola_contenedora.encolar(proceso_actual)
    
    proceso_actual = pool.buscar_proceso(207, 2)   
    #proceso_actual.mostrar_proceso()
    if proceso_actual:
        cola_contenedora.encolar(proceso_actual)
     
    """  print(cola_contenedora.cola_critica.get_tamanio())
    print(cola_contenedora.cola_no_critica.get_tamanio()) """

    cola_contenedora.ejecutar()
    cola_contenedora.ejecutar()
    cola_contenedora.ejecutar()
    cola_contenedora.ejecutar()
    proceso_actual = pool.buscar_proceso(100, 1)   
    #proceso_actual.mostrar_proceso()
    if proceso_actual:
        cola_contenedora.encolar(proceso_actual)
    cola_contenedora.ejecutar()
    cola_contenedora.ejecutar()
    cola_contenedora.ejecutar()
    cola_contenedora.ejecutar()
    cola_contenedora.ejecutar()
    proceso_actual = pool.buscar_proceso(102, 1)   
    #proceso_actual.mostrar_proceso()
    if proceso_actual:
        cola_contenedora.encolar(proceso_actual)
    cola_contenedora.ejecutar()
    cola_contenedora.ejecutar()
    cola_contenedora.ejecutar()
    cola_contenedora.ejecutar()
Unipe_CABA_Los_Mas_Grandes()