import proceso

class Arbol_procesos_criticos:
    def __init__(self):
        self.raiz = None
        self.tamanio = 0
    
    def get_tamanio(self):
        return self.tamanio

    def insertar_proceso_critico(self, proceso_nuevo):
        if self.raiz == None:
            self.raiz = proceso_nuevo
        else:
            actual = self.raiz
            padre = None 
            while actual != None:
                padre = actual
                if proceso_nuevo.get_id() < actual.get_id():
                    actual = actual.izquierda
                else: 
                    actual = actual.derecha
        
            if proceso_nuevo.get_id() < padre.get_id():
                padre.izquierda = proceso_nuevo
            else:
                padre.derecha = proceso_nuevo
        self.tamanio += 1
    
    def buscar_proceso_critico(self, id):
        actual = self.raiz
        while actual != None:
            if actual.get_id() == id:
                return actual
            elif id < actual.get_id():
                actual = actual.izquierda
            else:
                actual = actual.derecha
        return None
        
    def inorden(self, proceso_actual):
        if proceso_actual:
            self.inorden(proceso_actual.izquierda)
            print(str(proceso_actual.id) + " " + proceso_actual.nombre_proceso + " " + \
                  str(proceso_actual.valor_critico) + " " + str(proceso_actual.memoria) + " " + \
                  str(proceso_actual.tiempo_ejecucion) + " " + str(proceso_actual.parametros) \
                      ,end="\n")
            self.inorden(proceso_actual.derecha)