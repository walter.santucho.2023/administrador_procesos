import proceso

class Cola_de_Procesos:
    def __init__(self):
        self.ultimo = None
        self.primero = None
        self.tamanio = 0
    
    def get_tamanio(self):
        return self.tamanio

    def encolar(self, nuevo_proceso):
        if self.get_tamanio() == 0:
            self.primero = nuevo_proceso
            self.ultimo = nuevo_proceso
        else:
            nuevo_proceso.izquierda = self.ultimo
            nuevo_proceso.derecha = None
            apuntador = self.ultimo
            apuntador.derecha = nuevo_proceso
            self.ultimo = nuevo_proceso
        self.tamanio += 1
   
    def desencolar(self):
        if self.get_tamanio() > 0:
            apuntador = self.primero
            if apuntador.derecha != None:
                apuntador.derecha.izquierda = None
            self.primero = self.primero.derecha
            self.tamanio -= 1
            return apuntador

    