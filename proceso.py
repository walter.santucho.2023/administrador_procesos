class Proceso:
    def __init__(self):
        self.id = 0
        self.nombre_proceso = ''
        self.valor_critico = 0
        self.memoria = 0
        self.tiempo_ejecucion = 0
        self.parametros = {}
        #anterior
        self.izquierda = None
        #siguiente
        self.derecha = None
        
    def set_id(self, id):
        self.id = id

    def get_id(self):
        return self.id
    
    def set_valor_critico(self, valor_critico):
        self.valor_critico = valor_critico

    def get_valor_critico(self):
        return self.valor_critico
       
    def crear_copia(self):
        nuevo_proceso = Proceso()
        nuevo_proceso.id = self.id
        nuevo_proceso.nombre_proceso = self.nombre_proceso
        nuevo_proceso.valor_critico = self.valor_critico
        nuevo_proceso.memoria = self.memoria
        nuevo_proceso.tiempo_ejecucion = self.tiempo_ejecucion
        nuevo_proceso.parametros = self.parametros
        nuevo_proceso.izquierda = None
        nuevo_proceso.derecha = None
        return nuevo_proceso
        
    def mostrar_proceso(self):
        print(self.id)
        print(self.nombre_proceso)
        print(self.valor_critico)
        print(self.memoria)
        print(self.tiempo_ejecucion)
        print(self.parametros)